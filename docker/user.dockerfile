FROM python:3.9

RUN mkdir /user_app/
WORKDIR /user_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY data/users.csv data/users.csv
COPY src/common src/common
COPY src/user src/user

WORKDIR /user_app/src

ENV PYTHONPATH "/tmp/src/:$PYTHONPATH"

CMD ["python", "user/main.py"]