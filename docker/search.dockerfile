FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY data data
COPY src/common src/common
COPY src/search src/search

WORKDIR /search_app/src

ENV PYTHONPATH "/tmp/src/:$PYTHONPATH"

CMD ["python", "search/main.py"]