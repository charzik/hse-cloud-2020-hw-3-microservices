FROM python:3.9

RUN mkdir /metasearch_app/
WORKDIR /metasearch_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY data data
COPY src/common src/common
COPY src/tests src/tests
COPY src/metasearch src/metasearch

WORKDIR /metasearch_app/src

ENV PYTHONPATH "/tmp/src/:$PYTHONPATH"

CMD ["python", "metasearch/main.py"]