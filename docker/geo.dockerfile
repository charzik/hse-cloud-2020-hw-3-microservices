FROM python:3.9

RUN mkdir /geo_app/
WORKDIR /geo_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY data/geo.csv data/geo.csv
COPY src/common src/common
COPY src/geo src/geo

WORKDIR /geo_app/src

ENV PYTHONPATH "/tmp/src/:$PYTHONPATH"

CMD ["python", "geo/main.py"]