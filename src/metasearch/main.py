from common.data_source import CSV
from metasearch.http import Server
from metasearch.service import MetaSearchService
from settings import SEARCH_DOCUMENTS_DATA_FILES


def main():
    metasearch = MetaSearchService()
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
