import requests

from flask import Flask

from typing import List, Dict


class MetaSearchService:
    def __init__(self):
        pass

    def _get_user_data(self, user_id):
        response = requests.get('http://user:8001/get-user-data', params={'user_id': user_id})
        return response.json()['user_data']

    def _get_geo_data(self, ip):
        response = requests.get('http://geo:8002/get-geo-data', params={'ip': ip})
        return response.json()['geo_data']

    def _get_search(self, search_text, user_data, geo_data, limit):
        data = {
            'search_text': search_text, 
            'user_data': user_data, 
            'geo_data': geo_data, 
            'limit': limit
        }
        response = requests.post(
            'http://search:8003/search',
            headers={
                'Content-Type': 'application/json'
            },
            json=data
        )
        return response.json()

    def search(self, search_text, user_id, ip, limit=10) -> List[Dict]:
        user_data = self._get_user_data(user_id)  # {'gender': ..., 'age': ...}
        geo_data = self._get_geo_data(ip)  # {'region': ...}
        return self._get_search(search_text, user_data, geo_data, limit)
