from flask import Flask, request, jsonify

from geo.service import GeoService


class Server(Flask):
    def __init__(self, name: str, geo_service: GeoService):
        super().__init__(name)
        self._geo_service = geo_service
        urls = [
            ('/get-geo-data', self.get_geo_data, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_geo_data(self):
        ip = request.args.get('ip')
        response = {'geo_data': self._geo_service.get_geo_data(ip)}
        return jsonify(response)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8002, **kwargs)
