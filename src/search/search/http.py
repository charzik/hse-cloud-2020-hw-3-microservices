import json

from flask import Flask, request, jsonify

from search.service import SearchInShardsService, SimpleSearchService


class ServerSearch(Flask):
    def __init__(self, name: str, search: SearchInShardsService):
        super().__init__(name)
        self._search = search
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2], methods=['POST'])

    def search(self):
        body = request.get_json(force=True)
        search_text = body.get('search_text')
        user_data = body.get('user_data')
        geo_data = body.get('geo_data')
        limit = body.get('limit')
        df = self._search.get_search_data(search_text, user_data, geo_data, limit)
        return jsonify(df[self._search.DOCS_COLUMNS].to_dict('records'))


    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8003, **kwargs)


class ServerSimpleSearch(Flask):
    def __init__(self, name: str, search_shard: SimpleSearchService):
        super().__init__(name)
        self._search_shard = search_shard
        urls = [
            ('/search-in-shard', self.search_in_shard, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2], methods=['POST'])

    def search_in_shard(self):
        body = request.get_json(force=True)
        search_text = body.get('search_text')
        user_data = body.get('user_data')
        geo_data = body.get('geo_data')
        limit = body.get('limit')
        df = self._search_shard.get_search_data(search_text, user_data, geo_data, limit)
        return jsonify(df[self._search_shard.DOCS_COLUMNS_FULL].to_dict('records'))


    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8003, **kwargs)
