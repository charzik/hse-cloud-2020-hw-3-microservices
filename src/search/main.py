from common.data_source import CSV
from search.http import ServerSearch, ServerSimpleSearch
from search.service import SearchInShardsService, SimpleSearchService
from settings import SHARDS_HOSTS, SEARCH_DOCUMENTS_DATA_FILE


def main():
    if SEARCH_DOCUMENTS_DATA_FILE:
        search_shard = SimpleSearchService(CSV(SEARCH_DOCUMENTS_DATA_FILE))
        server = ServerSimpleSearch('search_shard', search_shard)
    else:
        search = SearchInShardsService(SHARDS_HOSTS)
        server = ServerSearch('search', search)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
