import os

BASE_DIR = os.sep.join(os.path.normpath(__file__).split(os.sep)[:-3])
BASE_DATA_DIR = os.path.join(BASE_DIR, 'data')
assert os.path.exists(BASE_DATA_DIR)


def datafile(file):
    if not file:
        return None
    
    return os.path.join(BASE_DATA_DIR, file)


SHARDS_HOSTS = ['simple_search_1', 'simple_search_2', 'simple_search_3']
SEARCH_DOCUMENTS_DATA_FILE = datafile(os.environ.get('SEARCH_DOCUMENTS_DATA_FILE', None))
